<?php

require_once 'connection.db.php';
class UserSubscriptionTest extends PHPUnit_Extensions_Selenium2TestCase
{
    public function setUp()
    {
        $this->setHost('localhost');
        $this->setPort(4444);
        $this->setBrowserUrl('http://cp.magenet.com');
        $this->setBrowser('firefox');
    }

    public function testTitle()
    {
        $title = "Sign in to MageNet";
        $this->url('http://cp.magenet.com');
        $this->assertEquals($title, $this->title());
    }

    public function testSubmitToSelf()
    {

        $this->url('http://cp.magenet.com');
        $form = $this->byId("login-form");
        $action = $form->attribute('action');
        $this->assertEquals('http://cp.magenet.com/user/login', $action);

        $this->byXPath("//*[@id='login-form-login']")->value('qa.test@mailforspam.com');
        $this->byId('login-form-password')->value('123456');

        $form->submit();
        $this->timeouts()->implicitWait(3000);
        sleep(3);
        $success = $this->byXPath('//*[@id="login-form"]//div[contains(text(), "Invalid login")]')->text();
        $this->assertEquals('Invalid login or password', $success);
    }
    public function testSubmitForm(){
        $dbcon = new QueryDb();
        $name = 'Alex';
        $email = 'qa.test.qa@mailforspam.com';
        $password = '123456';
        $title = 'Welcome to MageNet';
        $this->url('http://cp.magenet.com');
        $form = $this->byId("login-form");
        if(!$dbcon->getUserEmail($name)){
            $dbcon->insertEmail($email, $password);
        }
        $this->byXPath("//*[@id='login-form-login']")->value($dbcon->getUserEmail($name));
        $this->byId('login-form-password')->value($password);
        $form->submit();
        sleep(4);
        $this->assertEquals($title, $this->title());
    }

}