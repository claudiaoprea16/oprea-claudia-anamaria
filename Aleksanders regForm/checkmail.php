<?php


class checkmail {
    function IsVaildEmail($email) {
        $regexp = '/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/i';
        $result = 0;
        $mail = stripslashes($email);
        $mail = htmlspecialchars($mail);
        $mail = trim($mail);

        if (preg_match($regexp,$mail)) {
            $arr = explode("@" , $mail);
            if (getmxrr($arr[1],$mxhostsarr)==1) { $result = 1; } else { $result = 0; }
        } else { $result = 0; }

        return $result;
    }
}