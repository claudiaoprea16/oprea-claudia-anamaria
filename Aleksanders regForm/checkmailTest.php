<?php
require "Aleksanders regForm/checkmail.php";
class checkmailTest extends PHPUnit_Framework_TestCase
{
    public function testIsVaildEmail(){
        $em = new checkmail();
        $arr = array(
            'asderder.com',
            '#@%^%#$@#$@#.com',
            'plainaddress',
            'Joe Smith <email@domain.com>',
            '@domain.com',
            'email.domain.com',
            'email@domain@domain.com',
            'email@-domain.com',
            'あいうえお@domain.com',
            'email@domain.com (Joe Smith)',
            'email@domain',
            'email@domain..com'
        );
        foreach($arr as $arrseek){
            $this->assertEquals(0, $em->IsVaildEmail($arrseek));
        }
    }

}
