function validate_form(){
    $(".field-error").html("");
    var errors = false;

    var name = document.contact_form.contact_name.value;
    if (name == "" || name == 0 || name == " "){
        $("#name_f").html("*This field is required");
        errors = true;
    }

    if ( ( $(".male").prop("checked") == false ) && ( $(".female").prop("checked") == false ) )
    {
        $("#gender_f").html("*This field is required");
        errors = true;
    }

    var pattern = /^[a-z0-9_-]+@[a-z0-9-]+\.[a-z]{2,6}$/i;
    var mail = $('#email');
    if(mail.val() != ''){
        if(mail.val().search(pattern) != 0){
            $("#email_f").html("*Not valid email");
            errors = true;
        }
    }else{
        $("#email_f").html("*This field is required");
        errors = true;
    }


    var data = $("#password").val();
    var len = data.length;

    if(len < 1) {
        $("#password_f").html("*Password cannot be blank");
        errors = true;
    }

    if(data != $("#confirmpassword").val()) {
        $("#password_conf").html("*Password and Confirm Password don't match");
        errors = true;
    }

    if(errors == true){
        return false;
    }else{
        return true;
    }
}

