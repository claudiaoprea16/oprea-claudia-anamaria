Create a registration web form by fulfilling as many as possible of the following requirements 
- Collect the following data - name, gender, email, password
- Validate the data with html5 or Javascript
- Style the form with CSS as you like
- Submit that data to a PHP class to validate and return a response Success / Fail depending on validation
- Write some PHPUnit tests
- Store the data into a mysql table

**To complete the task please follow that procedure:**
- Fork that Repo: https://bitbucket.org/transformify/my-demo/
- Create a folder with your name and use it to commit your code as you work on it
- When you are ready open a pull request to submit the changes back

If you have questions please write to officeATtransformify.org

Note: Please do not change this file!