<?php

namespace SA\RegistrationBundle\Controller;

use SA\RegistrationBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RegisterController extends Controller   
{   
    public function indexAction(Request $request)
    {
        // we create an entity "user"
        $user = new User();
        
        // we build a form from this entity
        $form = $this->get('form.factory')->createBuilder('form',$user)
            ->add('name', 'text')
            ->add('gender', 'choice', array(
                'choices'  => array('male' => 'Male', 'female' => 'Female'),
                'required' => true,
            ))
            ->add('email', 'email')
            ->add('password', 'repeated', array(
                'type' => 'password',
                'first_name' => 'p1',
                'second_name' => 'p2',
                'first_options'  => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password')))
            ->add('save', 'submit')
            ->getForm();
        
        // we link the request to the form
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            // the request is a POST and is valid
            $validator = $this->get('validator');
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            
            // User created
            return new Response("New user created!");
        }
        
        // Form data not valid  or GET request
        return $this->render('SARegistrationBundle:Register:index.html.twig',
                             array ('form' => $form->createView()));
    }
}