<?php

namespace SA\RegistrationBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegisterControllerTest extends WebTestCase
{
    
    public function setUp()
    {
        //we recreate a new DB before every test
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();
        
        $schemaTool = new \Doctrine\ORM\Tools\SchemaTool($em);
        
        $mdf = $em->getMetadataFactory();
        $classes = $mdf->getAllMetadata();
        
        $schemaTool->dropDatabase();
        $schemaTool->createSchema($classes);
    }
    
    // helper function to add a user in the DB
    protected function addDBEntry (
                                $name = "Sylvain",
                                $gender = "male",
                                $email = "s.auguy@gmail.com",
                                $password = "12345678"
                                   ) {
        $client = static::createClient();
        
        $crawler = $client->request('GET', '/');
        
        $form = $crawler->selectButton('Save')->form();
        $form['form[name]'] = $name;
        $form['form[gender]'] = $gender;
        $form['form[email]'] = $email;
        $form['form[password]'] = $password;
        $crawler = $client->submit($form);
        return $client;
    }
    
    public function testPassword() {
        $client = $this->addDBEntry('S', "male", "s.auguy@gmail.com", "123456");
        $this->assertContains('Password too weak', $client->getResponse()->getContent());
    }
    
    public function testEmail() {
        $client = $this->addDBEntry('S', "male", "myemail.com");
        $this->assertContains('Invalid email address', $client->getResponse()->getContent());
    }
    
    public function testGender() {
        $this->setExpectedException('InvalidArgumentException');
        $client = $this->addDBEntry('S', 'boy');
    }
    
    public function testAddUserTwice(){
        $client = $this->addDBEntry();
        $this->assertContains('New user created!', $client->getResponse()->getContent());
        $client = $this->addDBEntry();
        $this->assertContains('This email is already registered', $client->getResponse()->getContent());
    }
    
    public function testAddUser(){
        $client = $this->addDBEntry();
        $this->assertContains('New user created!', $client->getResponse()->getContent());
    }
    
    public function testFormDisplay()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
        
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('SA\RegistrationBundle\Controller\RegisterController::indexAction',
                            $client->getRequest()->attributes->get('_controller'));
        $this->assertContains('Sign up', $crawler->filter('h2')->text());
        $this->assertTrue($crawler
                            ->filter('form[name=form]')
                                ->form()
                                    ->has('form[name]'));
        $this->assertTrue($crawler
                            ->filter('form[name=form]')
                                ->form()
                                    ->has('form[gender]'));
        $this->assertTrue($crawler
                            ->filter('form[name=form]')
                                ->form()
                                    ->has('form[email]'));
        $this->assertTrue($crawler
                            ->filter('form[name=form]')
                                ->form()
                                    ->has('form[password]'));
    }
    
}
