<?php

namespace cengizBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use cengizBundle\Entity\User;
use cengizBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{

    public function indexAction(Request $request){

		$em = $this->getDoctrine()->getManager();		   
		$users = $em->getRepository("cengizBundle:User")->findAll();
		
		$user = new User();
		$form = $this->createForm(new UserType(), $user);
		
		$request = $this->get('request');
		$form->handleRequest($request);
		
		if ($request->getMethod() == 'POST')
		{
			if ($form->isValid())
			{
				$adduser = new User();

				$adduser->setName($form->get('name')->getData());
				$adduser->setGender($form->get('gender')->getData());
				$adduser->setEmail($form->get('email')->getData());
				$adduser->setPassword($form->get('password')->getData());
	
				$em = $this->getDoctrine()->getManager();
				$em->persist($adduser);
				$em->flush();

				$this->get('session')->getFlashBag()->add('msg', 'Registration complete!');

				return $this->redirectToRoute('cengiz_homepage');
			}
			return $this->render('cengizBundle:Default:index.html.twig', array('form'=>$form->createView(), 'users' => $users));
		}
		return $this->render('cengizBundle:Default:index.html.twig', array('form'=>$form->createView(), 'users' => $users));
    }
}