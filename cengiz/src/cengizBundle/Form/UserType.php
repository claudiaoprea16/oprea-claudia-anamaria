<?php

namespace cengizBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('name', 'text', array('label' => 'Name', 'attr' => array('class' => 'form-control')))

->add('gender', 'choice', array('label' => 'Gender', 'choices' => array('' => 'select', 'female' => 'female', 'man' => 'man'),
 'attr' => array('class' => 'form-control')))
				->add('email', 'email', array('label' => 'Email', 'attr' => array('class' => 'form-control')))
				->add('password', 'repeated', array(
					'type' => 'password', 
					'invalid_message' => '*The password fields must match!', 
					'options' => array('required' => true), 
					'first_options'  => array('label' => 'Password', 'attr' => array('class' => 'form-control')), 
					'second_options' => array('label' => 'Password Confirm', 'attr' => array('class' => 'form-control')),
				))
				->add('register', 'submit', array('label' => 'Save', 'attr' => array('class' => 'btn btn-primary pull-right')));
	}

	public function getName()
	{
		return 'user';
	}


}