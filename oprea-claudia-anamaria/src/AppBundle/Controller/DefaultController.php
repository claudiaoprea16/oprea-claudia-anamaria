<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

use Doctrine\ORM\Query\ResultSetMapping;

use AppBundle\Entity\Person;



class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $person = new Person();
        $form = $this -> createFormBuilder($person)
            ->add('name',TextType::class)
            ->add('gender',TextType::class)
            ->add('email',EmailType::class)
            ->add('password',PasswordType::class)
            ->add('save',SubmitType::class)
            ->getForm()
        ;

        $form->handleRequest($request);
        $FormResponse = new ValidateFormResponse();
        $result = $FormResponse -> Run($form);
        
        if($result) {
            $em = $this -> getDoctrine()->getManager();
            $em -> persist($person);
            $em -> flush();
        }
    
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'form' => $form->createView(),
        ]);

    }
}

Class ValidateFormResponse {
    public function Run($form) {
       if ($form->isSubmitted() && $form->isValid()) {
            return true;
        }
        return false;
    }
}