<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Person
 *
 * @ORM\Table(name="person")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonRepository")
 */
class Person
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\Column(type="string",length=100)
    */
    protected $name;
    /**
    * @param mixed $name
    */

    /**
    * @ORM\Column(type="string",length=100)
    */
    protected $gender;
    /**
    * @param mixed $gender
    */

    /**
    * @ORM\Column(type="string",length=100)
    */
    protected $email;
    /**
    * @param mixed $email
    */

    /**
    * @ORM\Column(type="string",length=100)
    */
    protected $password;
    /**
    * @param mixed $password
    */

    public function setName($name) {
        $this->name=$name;
        return $this;
    }
    /**
    * @return mixed
    */
    public function getName() {
        return $this->name;
    }

    public function setGender($gender) {
        $this->gender=$gender;
        return $this;
    }
    /**
    * @return mixed
    */
    public function getGender() {
        return $this->gender;
    }

    public function setEmail($email) {
        $this->email=$email;
        return $this;
    }
    /**
    * @return mixed
    */
    public function getEmail() {
        return $this->email;
    }

    public function setPassword($password) {
        $this->password=$password;
        return $this;
    }
    /**
    * @return mixed
    */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}

