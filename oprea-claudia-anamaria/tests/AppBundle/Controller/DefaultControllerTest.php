<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class DefaultControllerTest extends WebTestCase
{
	/**
     * @dataProvider additionProvider
     */

    public function testAdd($a, $b, $expected) {
        $this->assertEquals($expected, $a + $b);
    }

    public function additionProvider() {
    	$addition = new Addition();
        return $addition -> getAddition();
    }

    /**
     * @dataProvider multipliedProvider
     */

    public function testMultiplied($a, $b, $expected) {
        $this->assertEquals($expected, $a * $b);
    }

    public function multipliedProvider() {
        return array(
          array(0, 0, 0),
          array(0, 1, 0),
          array(1, 1, 1)
        );
    }

    public function testGetMessageByTime() {
    	$time = date('H:i:s');
    	if($time < '9:00:00')
    		$message = 'Good Morning!';
    	else
    		$message = 'Good Afternoon!';
    	$this->assertTrue(true);
    	return $message;
    }

    public function testSumOfRandomNumbers() {
    	$firstNumber = rand();
    	$secondNumber = rand();
    	$sumOfTheNumber = $firstNumber + $secondNumber;
    	$this->assertTrue(true);
    	return $sumOfTheNumber;
    }

    public function testPost() {
        $client = static::createClient();
        $crawler = $client->request('GET', '/post/hello-world');
        $this->assertGreaterThan(
            -1,
            $crawler->filter('html:contains("Hello World")')->count()
        );
    }

    public function testPush() {
        $stack = array();
        $this->assertEquals(0, count($stack));
        array_push($stack, 'foo');
        $this->assertEquals('foo', $stack[count($stack)-1]);
        $this->assertEquals(1, count($stack));
        $this->assertEquals('foo', array_pop($stack));
        $this->assertEquals(0, count($stack));
    }

}
class Addition {
    public function getAddition() {
    	return array(
          array(0, 0, 0),
          array(0, 1, 1),
          array(1, 0, 1)
        );
    }
}