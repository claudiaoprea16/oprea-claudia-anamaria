<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_b0d502a750abb837b4cecfb411c77a65c30972f1a1fe69280b63dd477559c08b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8c837b78e1ec46533baa172a8a81b22de2e2ef098eeed293af6050ccf1d6884b = $this->env->getExtension("native_profiler");
        $__internal_8c837b78e1ec46533baa172a8a81b22de2e2ef098eeed293af6050ccf1d6884b->enter($__internal_8c837b78e1ec46533baa172a8a81b22de2e2ef098eeed293af6050ccf1d6884b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8c837b78e1ec46533baa172a8a81b22de2e2ef098eeed293af6050ccf1d6884b->leave($__internal_8c837b78e1ec46533baa172a8a81b22de2e2ef098eeed293af6050ccf1d6884b_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_8f2e8829371f8a943d4113cca48ce8fd4e1218621d0c1f63f6dc86916e7b9b88 = $this->env->getExtension("native_profiler");
        $__internal_8f2e8829371f8a943d4113cca48ce8fd4e1218621d0c1f63f6dc86916e7b9b88->enter($__internal_8f2e8829371f8a943d4113cca48ce8fd4e1218621d0c1f63f6dc86916e7b9b88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_8f2e8829371f8a943d4113cca48ce8fd4e1218621d0c1f63f6dc86916e7b9b88->leave($__internal_8f2e8829371f8a943d4113cca48ce8fd4e1218621d0c1f63f6dc86916e7b9b88_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_abc3307a0e29330858c295798260799710634432b03f0c1408e53ea9e3c30102 = $this->env->getExtension("native_profiler");
        $__internal_abc3307a0e29330858c295798260799710634432b03f0c1408e53ea9e3c30102->enter($__internal_abc3307a0e29330858c295798260799710634432b03f0c1408e53ea9e3c30102_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_abc3307a0e29330858c295798260799710634432b03f0c1408e53ea9e3c30102->leave($__internal_abc3307a0e29330858c295798260799710634432b03f0c1408e53ea9e3c30102_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_36bc8af16a81b8257e204ee63fa225694ca38828ef3a9fb9daabab24858abb42 = $this->env->getExtension("native_profiler");
        $__internal_36bc8af16a81b8257e204ee63fa225694ca38828ef3a9fb9daabab24858abb42->enter($__internal_36bc8af16a81b8257e204ee63fa225694ca38828ef3a9fb9daabab24858abb42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_36bc8af16a81b8257e204ee63fa225694ca38828ef3a9fb9daabab24858abb42->leave($__internal_36bc8af16a81b8257e204ee63fa225694ca38828ef3a9fb9daabab24858abb42_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
