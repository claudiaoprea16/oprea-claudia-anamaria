<?php

/* default/index.html.twig */
class __TwigTemplate_1fb73bfe0ebcce33e8d21e330cbc12c396bbb666f51029b90f264447dd93801e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_042464a8e983e86755392e276e42df7a4196c7764dfb053e5b7c1d2566e836be = $this->env->getExtension("native_profiler");
        $__internal_042464a8e983e86755392e276e42df7a4196c7764dfb053e5b7c1d2566e836be->enter($__internal_042464a8e983e86755392e276e42df7a4196c7764dfb053e5b7c1d2566e836be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_042464a8e983e86755392e276e42df7a4196c7764dfb053e5b7c1d2566e836be->leave($__internal_042464a8e983e86755392e276e42df7a4196c7764dfb053e5b7c1d2566e836be_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_8dccbdd1c723a2ff77b3e201935be87a64354172047f1e2677b1cae3a867bf19 = $this->env->getExtension("native_profiler");
        $__internal_8dccbdd1c723a2ff77b3e201935be87a64354172047f1e2677b1cae3a867bf19->enter($__internal_8dccbdd1c723a2ff77b3e201935be87a64354172047f1e2677b1cae3a867bf19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div id=\"wrapper\">
    <div id=\"title\"><h1>Claudia's Form</h1></div>
        <div class=\"container\">
            ";
        // line 7
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
            <div class=\"form-group\">
                ";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'label');
        echo "
                ";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'errors');
        echo "
                ";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>

            <div class=\"form-group\">
                ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gender", array()), 'label');
        echo "
                ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gender", array()), 'errors');
        echo "
                ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gender", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>

            <div class=\"form-group\">
                ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'label');
        echo "
                ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'errors');
        echo "
                ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>

            <div class=\"form-group\">
                ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password", array()), 'label');
        echo "
                ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password", array()), 'errors');
        echo "
                ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
            ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "save", array()), 'widget');
        echo "
            ";
        // line 32
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    </div>
";
        
        $__internal_8dccbdd1c723a2ff77b3e201935be87a64354172047f1e2677b1cae3a867bf19->leave($__internal_8dccbdd1c723a2ff77b3e201935be87a64354172047f1e2677b1cae3a867bf19_prof);

    }

    // line 36
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_d0c015416d67fc8a5e9bfaae9350cbfcbffa8dc2cee78b56a854984e0ab7a856 = $this->env->getExtension("native_profiler");
        $__internal_d0c015416d67fc8a5e9bfaae9350cbfcbffa8dc2cee78b56a854984e0ab7a856->enter($__internal_d0c015416d67fc8a5e9bfaae9350cbfcbffa8dc2cee78b56a854984e0ab7a856_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 37
        echo "<style>
    body { background: #F5F5F5; font: 18px/1.5 sans-serif; }
    h1, h2 { line-height: 1.2; margin: 0 0 .5em; }
    h1 { font-size: 36px; }
    h2 { font-size: 21px; margin-bottom: 1em; }
    p { margin: 0 0 1em 0; }
    a { color: #0000F0; }
    a:hover { text-decoration: none; }
    code { background: #F5F5F5; max-width: 100px; padding: 2px 6px; word-wrap: break-word; }
    #wrapper { background: #FFF; margin: 1em auto; max-width: 800px; width: 95%; }
    #container { padding: 2em; }
    #welcome, #status { margin-bottom: 2em; }
    #welcome h1 span { display: block; font-size: 75%; }
    #icon-status, #icon-book { float: left; height: 64px; margin-right: 1em; margin-top: -4px; width: 64px; }
    #icon-book { display: none; }
    .form-control {
    width: 600px;
    }
    #title {
    padding: 30px;
    }
    #form_save {
        padding:5px 10px 5px 10px;
        background-color: #0080FF;
        border: 2px solid #a1a1a1;
        border-radius: 15px;
        color: #FFF;
    }
    @media (min-width: 768px) {
        #wrapper { width: 80%; margin: 2em auto; }
        #icon-book { display: none; }
        #status a, #next a { display: block; }

        @-webkit-keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        @keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        .sf-toolbar { opacity: 0; -webkit-animation: fade-in 1s .2s forwards; animation: fade-in 1s .2s forwards;}
    }
</style>
";
        
        $__internal_d0c015416d67fc8a5e9bfaae9350cbfcbffa8dc2cee78b56a854984e0ab7a856->leave($__internal_d0c015416d67fc8a5e9bfaae9350cbfcbffa8dc2cee78b56a854984e0ab7a856_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 37,  123 => 36,  113 => 32,  109 => 31,  104 => 29,  100 => 28,  96 => 27,  89 => 23,  85 => 22,  81 => 21,  74 => 17,  70 => 16,  66 => 15,  59 => 11,  55 => 10,  51 => 9,  46 => 7,  41 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <div id="wrapper">*/
/*     <div id="title"><h1>Claudia's Form</h1></div>*/
/*         <div class="container">*/
/*             {{form_start(form)}}*/
/*             <div class="form-group">*/
/*                 {{form_label(form.name)}}*/
/*                 {{form_errors(form.name)}}*/
/*                 {{form_widget(form.name, {'attr': {'class':'form-control'}}   )}}*/
/*             </div>*/
/* */
/*             <div class="form-group">*/
/*                 {{form_label(form.gender)}}*/
/*                 {{form_errors(form.gender)}}*/
/*                 {{form_widget(form.gender, {'attr': {'class':'form-control'}} )}}*/
/*             </div>*/
/* */
/*             <div class="form-group">*/
/*                 {{form_label(form.email)}}*/
/*                 {{form_errors(form.email)}}*/
/*                 {{form_widget(form.email, {'attr': {'class':'form-control'}} )}}*/
/*             </div>*/
/* */
/*             <div class="form-group">*/
/*                 {{form_label(form.password)}}*/
/*                 {{form_errors(form.password)}}*/
/*                 {{form_widget(form.password, {'attr': {'class':'form-control'}} )}}*/
/*             </div>*/
/*             {{form_widget(form.save)}}*/
/*             {{form_end(form)}}*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* <style>*/
/*     body { background: #F5F5F5; font: 18px/1.5 sans-serif; }*/
/*     h1, h2 { line-height: 1.2; margin: 0 0 .5em; }*/
/*     h1 { font-size: 36px; }*/
/*     h2 { font-size: 21px; margin-bottom: 1em; }*/
/*     p { margin: 0 0 1em 0; }*/
/*     a { color: #0000F0; }*/
/*     a:hover { text-decoration: none; }*/
/*     code { background: #F5F5F5; max-width: 100px; padding: 2px 6px; word-wrap: break-word; }*/
/*     #wrapper { background: #FFF; margin: 1em auto; max-width: 800px; width: 95%; }*/
/*     #container { padding: 2em; }*/
/*     #welcome, #status { margin-bottom: 2em; }*/
/*     #welcome h1 span { display: block; font-size: 75%; }*/
/*     #icon-status, #icon-book { float: left; height: 64px; margin-right: 1em; margin-top: -4px; width: 64px; }*/
/*     #icon-book { display: none; }*/
/*     .form-control {*/
/*     width: 600px;*/
/*     }*/
/*     #title {*/
/*     padding: 30px;*/
/*     }*/
/*     #form_save {*/
/*         padding:5px 10px 5px 10px;*/
/*         background-color: #0080FF;*/
/*         border: 2px solid #a1a1a1;*/
/*         border-radius: 15px;*/
/*         color: #FFF;*/
/*     }*/
/*     @media (min-width: 768px) {*/
/*         #wrapper { width: 80%; margin: 2em auto; }*/
/*         #icon-book { display: none; }*/
/*         #status a, #next a { display: block; }*/
/* */
/*         @-webkit-keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }*/
/*         @keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }*/
/*         .sf-toolbar { opacity: 0; -webkit-animation: fade-in 1s .2s forwards; animation: fade-in 1s .2s forwards;}*/
/*     }*/
/* </style>*/
/* {% endblock %}*/
/* */
