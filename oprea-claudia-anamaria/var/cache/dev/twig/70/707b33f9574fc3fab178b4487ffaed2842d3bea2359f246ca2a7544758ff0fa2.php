<?php

/* base.html.twig */
class __TwigTemplate_a3237722052ea40b32165596a5b583a093635b1a784e94e5e09abb12f5b3e174 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9c3cf403b2052a23a4f1ae66fb85c7b939d75717142a368eee0da8a2b2597554 = $this->env->getExtension("native_profiler");
        $__internal_9c3cf403b2052a23a4f1ae66fb85c7b939d75717142a368eee0da8a2b2597554->enter($__internal_9c3cf403b2052a23a4f1ae66fb85c7b939d75717142a368eee0da8a2b2597554_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>Form</title>
        <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">
        ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 8
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body> 
        ";
        // line 11
        $this->displayBlock('body', $context, $blocks);
        // line 12
        echo "            <!--";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "-->
            
        </div>
    </body>
</html>
";
        
        $__internal_9c3cf403b2052a23a4f1ae66fb85c7b939d75717142a368eee0da8a2b2597554->leave($__internal_9c3cf403b2052a23a4f1ae66fb85c7b939d75717142a368eee0da8a2b2597554_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_7ed786ad64f7da231c0940db47c82a66d87b259f219668e939b896efd52fd003 = $this->env->getExtension("native_profiler");
        $__internal_7ed786ad64f7da231c0940db47c82a66d87b259f219668e939b896efd52fd003->enter($__internal_7ed786ad64f7da231c0940db47c82a66d87b259f219668e939b896efd52fd003_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_7ed786ad64f7da231c0940db47c82a66d87b259f219668e939b896efd52fd003->leave($__internal_7ed786ad64f7da231c0940db47c82a66d87b259f219668e939b896efd52fd003_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_1c789942f543c7bfc03a91354ae247ba54f4d165b62ce71e27417dcab2f84ee5 = $this->env->getExtension("native_profiler");
        $__internal_1c789942f543c7bfc03a91354ae247ba54f4d165b62ce71e27417dcab2f84ee5->enter($__internal_1c789942f543c7bfc03a91354ae247ba54f4d165b62ce71e27417dcab2f84ee5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_1c789942f543c7bfc03a91354ae247ba54f4d165b62ce71e27417dcab2f84ee5->leave($__internal_1c789942f543c7bfc03a91354ae247ba54f4d165b62ce71e27417dcab2f84ee5_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 11,  57 => 7,  43 => 12,  41 => 11,  34 => 8,  32 => 7,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>Form</title>*/
/*         <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body> */
/*         {% block body %}{% endblock %}*/
/*             <!--{{form(form)}}-->*/
/*             */
/*         </div>*/
/*     </body>*/
/* </html>*/
/* */
