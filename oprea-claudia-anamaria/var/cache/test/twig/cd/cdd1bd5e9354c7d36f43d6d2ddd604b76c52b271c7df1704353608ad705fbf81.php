<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_11eba0a44617c522c4d79a67b8e1640679fed18499409527f2b6f1791b89f4af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_545a63b44746538f8e86f01f9934f79859339c5d3ab2cc077ca56dc8895f38bc = $this->env->getExtension("native_profiler");
        $__internal_545a63b44746538f8e86f01f9934f79859339c5d3ab2cc077ca56dc8895f38bc->enter($__internal_545a63b44746538f8e86f01f9934f79859339c5d3ab2cc077ca56dc8895f38bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_545a63b44746538f8e86f01f9934f79859339c5d3ab2cc077ca56dc8895f38bc->leave($__internal_545a63b44746538f8e86f01f9934f79859339c5d3ab2cc077ca56dc8895f38bc_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_f83df3c4f472a19969f967ac6b3db13435a8a02a50cc47a19a8077eca84c25a6 = $this->env->getExtension("native_profiler");
        $__internal_f83df3c4f472a19969f967ac6b3db13435a8a02a50cc47a19a8077eca84c25a6->enter($__internal_f83df3c4f472a19969f967ac6b3db13435a8a02a50cc47a19a8077eca84c25a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_f83df3c4f472a19969f967ac6b3db13435a8a02a50cc47a19a8077eca84c25a6->leave($__internal_f83df3c4f472a19969f967ac6b3db13435a8a02a50cc47a19a8077eca84c25a6_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_519be8048a75e665cdcbb1534f5b87d6da3b7f07856099802c144e2d2ba0cc8f = $this->env->getExtension("native_profiler");
        $__internal_519be8048a75e665cdcbb1534f5b87d6da3b7f07856099802c144e2d2ba0cc8f->enter($__internal_519be8048a75e665cdcbb1534f5b87d6da3b7f07856099802c144e2d2ba0cc8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_519be8048a75e665cdcbb1534f5b87d6da3b7f07856099802c144e2d2ba0cc8f->leave($__internal_519be8048a75e665cdcbb1534f5b87d6da3b7f07856099802c144e2d2ba0cc8f_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_e112552b8728e262009d8199ff0b47a7a95f43775b5175f40dea2cb20b5dabf0 = $this->env->getExtension("native_profiler");
        $__internal_e112552b8728e262009d8199ff0b47a7a95f43775b5175f40dea2cb20b5dabf0->enter($__internal_e112552b8728e262009d8199ff0b47a7a95f43775b5175f40dea2cb20b5dabf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_e112552b8728e262009d8199ff0b47a7a95f43775b5175f40dea2cb20b5dabf0->leave($__internal_e112552b8728e262009d8199ff0b47a7a95f43775b5175f40dea2cb20b5dabf0_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
