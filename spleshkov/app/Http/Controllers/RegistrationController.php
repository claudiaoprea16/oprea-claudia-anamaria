<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserFormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Auth\Guard;


class RegistrationController extends Controller
{
    /**
     * the model instance
     * @var User
     */
    protected $user;
    /**
     * The Guard implementation.
     *
     * @var Authenticator
     */
    protected $auth;

    /**
     * Create a new authentication controller instance.
     *
     * @param  Authenticator  $auth
     * @return void
     */
    public function __construct(Guard $auth, User $user)
    {
        $this->user = $user;
        $this->auth = $auth;

        $this->middleware('guest', ['except' => ['getLogout']]);
    }

    /**
     * Show login page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLogin()
    {
        return view('registration.login');
    }

    public function postLogin(Request $request)
    {
        // Mock login to test fail attempt

        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);
        $email = $request->input('email');
        $password = $request->input('password');

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            Session::flash('message', 'You sucessfully logged in');
            return redirect('registration/login');
        }else{
            Session::flash('message', 'Fail!');
            return redirect('registration/login');
        }
        
    }

    /**
     * Show the form for creating a new user.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        return view('registration.create');
    }

    /**
     * Store a newly created user.
     * Injecting UserFormRequest to validate form post data
     * @param  UserFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(UserFormRequest $request)
    {
        $user = new User([
            'name'      => $request->get('name'),
            'gender'    => $request->get('gender'),
            'email'     => $request->get('email'),
            'password'  => Hash::make($request->get('password'))
        ]);

        if($user->save()){
            return redirect('/');
        }else{
            return[
                'success' => false,
                'text' =>'Can not register user.'
            ];
        }
    }

}
