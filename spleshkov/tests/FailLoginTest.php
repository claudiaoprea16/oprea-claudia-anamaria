<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FailLoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoginFailure()
    {
        $this->visit('registration/login')
            ->type('test@email1.com', 'email')
            ->type('testtestetst', 'password')
            ->press('Submit')
            ->see('Fail!');
    }
}
